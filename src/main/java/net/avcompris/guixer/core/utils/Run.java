package net.avcompris.guixer.core.utils;

import net.avcompris.binding.annotation.XPath;

@XPath("/test")
public interface Run {

	@XPath("@guixerVersion")
	String getGuixerVersion();

	@XPath("@startedAtMs")
	long getStartedAtMs();

	@XPath("command")
	Command[] getCommands();

	interface Command {

		@XPath("@actionShortDescription")
		String getActionShortDescription();

		@XPath("step")
		Step[] getSteps();
	}

	interface Step {

		@XPath("startedAtMs")
		long getStartedAtMs();

		@XPath("endedAtMs")
		long getEndedAtMs();

		@XPath("elapsedMs")
		int getElapsedMs();

		@XPath("index")
		int getIndex();

		@XPath("literal")
		String getLiteral();
	}
}
